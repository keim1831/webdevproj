const express = require('express');
const session = require('express-session');
const expressHandlebars = require('express-handlebars');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const formidableMiddleware = require('express-formidable');
const router = require('./router/root.js');
const apiRouter = require('./router/api.js');


const app = express();
const port = 3000;

const handlebars = expressHandlebars({
  extname: '.hbs',
  helpers: {
    ifEquals: (arg1, arg2, options) => ((arg1 === arg2) ? options.fn(this) : options.inverse(this)),
  },
});

app.engine('.hbs', handlebars);
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static('static'));
app.use(morgan('tiny'));

app.use(session({
  secret: '142e6ecf42884a256',
  resave: false,
  saveUninitialized: true,
}));

app.use('/api', bodyParser.json(), apiRouter);

app.use(formidableMiddleware());
app.use('/', router);

app.listen(port, () => console.log(`listening at http://localhost:${port}`));
