function authorize(szerepek = ['user', 'admin']) {
  return (req, res, next) => {
    if (!req.session.szerepkor) {
      res.status(401).send('You are not logged in');
    } else if (!szerepek.includes(req.session.szerepkor)) {
      res.status(401).send('You do not have permission to access this endpoint');
    } else {
      next();
    }
  };
}

module.exports = authorize;
