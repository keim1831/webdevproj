const express = require('express');
const helpers = require('../helpers/helpers.js');
const dataAccess = require('../data_access/dataAccess.js');
const authorize = require('../middleware/authorize.js');

const router = express.Router();

// betolti a felhasznalokat es a rendezvenyeket a csatlakozasnal
function renderJoiningAnEvent(req, res, msg) {
  dataAccess.getUsers((err, felhasznalo) => {
    if (err) res.send(err);
    else {
      dataAccess.getEvents((error, rendezveny) => {
        if (error) res.send(error);
        else {
          res.render('connect',
            {
              session: req.session, felhasznalo, rendezveny, uzenet: msg,
            });
        }
      });
    }
  });
}

// ha csatlakozas / torles form
router.get('/', authorize(['admin']), (req, res) => {
  renderJoiningAnEvent(req, res, ' ');
});

// szervezo csatlakozasa / torlese
router.post('/', authorize(['admin']), (req, res) => {
  if (req.fields.muvelet === 'connect') {
    helpers.insertOrganizer(req.fields, (status, msg) => {
      renderJoiningAnEvent(req, res, msg);
    });
  } else {
    helpers.deleteOrganizer(req.fields, (status, message) => {
      renderJoiningAnEvent(req, res, message);
    });
  }
});

module.exports = router;
