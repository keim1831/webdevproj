const express = require('express');
const dataAccess = require('../data_access/dataAccess.js');
const authorize = require('../middleware/authorize.js');

const router = express.Router();


router.delete('/organisational-relationships/:id', authorize(), (req, res) => {
  dataAccess.deleteOrganizer(req.session.nev, req.params.id, (err) => {
    if (err) res.status(500).send('Db hiba!');
    else res.status(200).send();
  });
});

router.post('/organisational-relationships', authorize(), (req, res) => {
  dataAccess.insertOrganizer(req.session.nev, req.body.rendezvenyId, (err) => {
    if (err) res.status(500).send('Db hiba!');
    else res.status(200).send();
  });
});

router.get('/organisational-relationships', (req, res) => {
  dataAccess.getEventOrganizer(req.query.rendezvenyId, (err, result) => {
    if (err) {
      res.status(500).send('DB hiba!');
      console.error(err);
    } else {
      const nevek = result.map((x) => x.nev);
      res.json(nevek);
    }
  });
});

router.get('/events', (req, res) => {
  if (req.query.helyszin) {
    dataAccess.getEventByLocation(req.query.helyszin, (err, rendezveny) => {
      if (err) res.status(500).send('Db hiba!');
      else res.status(200).json(rendezveny);
    });
  } else {
    dataAccess.getEvents((err, rendezveny) => {
      if (err) res.status(500).send('Db hiba!');
      else res.status(200).json(rendezveny);
    });
  }
});


router.get('/events/:id', (req, res) => {
  dataAccess.getEvent(req.params.id, (err, events) => {
    if (err) res.status(500).send('DB hiba');
    else if (!events.length) res.status(404).send('NOT FOUND!');
    else {
      res.status(200).json(events[0]);
    }
  });
});

router.post('/events', (req, res) => {
  const {
    nev, kezdet, veg, helyszin,
  } = req.body;
  dataAccess.insertEvent(nev, kezdet, veg, helyszin, (err, result) => {
    if (err) res.status(500).send('DB hiba');
    else res.status(201).location(`${req.fullUrl}/${result.insertId}`).send({ id: result.insertId });
  });
});

router.put('/events/:id', (req, res) => {
  const {
    nev, kezdet, veg, helyszin,
  } = req.body;
  dataAccess.updateEvent(req.params.id, nev, kezdet, veg, helyszin, (err) => {
    if (err) res.status(500).send('DB hiba');
    else {
      res.status(200).json({
        id: req.params.id, nev, kezdet, veg, helyszin,
      });
    }
  });
});

router.delete('/events/:id', (req, res) => {
  dataAccess.deleteEvent(req.params.id, (err, result) => {
    if (err) res.status(500).send('DB hiba!');
    else if (!result.affectedRows) res.status(404).send('NOT FOUND!');
    else res.status(200).send();
  });
});

router.use((req, res) => res.status(404).send('NOT FOUND!'));
module.exports = router;
