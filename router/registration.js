const express = require('express');
const { hashPassord } = require('../helpers/passwords.js');
const dataAccess = require('../data_access/dataAccess.js');


const router = express.Router();


function renderRegistration(req, res, uzenet) {
  res.render('registration', { session: req.session, uzenet });
}


router.get('/', (req, res) => {
  if (req.session.nev) {
    res.redirect('/');
    return;
  }
  renderRegistration(req, res);
});

function addUser(req, res, nev, jelszo, szerepkor) {
  hashPassord(jelszo, (err, salt, hash) => {
    if (err) renderRegistration(req, res, 'Nem vart hiba!');
    else {
      dataAccess.insertUser(nev, hash, salt, szerepkor, (error) => {
        if (error) renderRegistration(req, res, 'Db hiba!');
        else res.redirect('/');
      });
    }
  });
}

router.post('/', (req, res) => {
  if (req.session.nev) {
    res.redirect('/');
    return;
  }
  const { nev, jelszo, szerepkor } = req.fields;
  dataAccess.usernameIsTaken(nev, (err, nevFoglalt) => {
    if (err) renderRegistration(req, res, 'Db hiba!');
    else if (nevFoglalt) renderRegistration(req, res, 'A felhasznalonev foglalt!');
    else addUser(req, res, nev, jelszo, szerepkor);
  });
});

module.exports = router;
