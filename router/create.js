const express = require('express');
const helpers = require('../helpers/helpers.js');
const authorize = require('../middleware/authorize.js');

const router = express.Router();


function renderCreateEvent(req, res, uzenet) {
  res.render('create', { session: req.session, uzenet });
}

// rendezveny letrehozasahoz kapcsolodo form betoltese
router.get('/', authorize(['admin']), (req, res) => {
  renderCreateEvent(req, res);
});

// rendezveny letrehozasa, elkuldi az adatokat, majd annak fuggvenyeben,
// hogy letrehozhattuk a rendezveny (mert meg nem letezett az adatbazisunkban,
// vagy megfelelo volt az intervallum) atiranyit a megfelelo oldalra
router.post('/', authorize(['admin']), (req, res) => {
  helpers.insertEvent(req.fields, (status, result) => {
    if (status === 200) {
      res.redirect('/');
    } else {
      renderCreateEvent(req, res, result);
    }
  });
});

module.exports = router;
