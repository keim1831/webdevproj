const express = require('express');
const { checkPassword } = require('../helpers/passwords.js');
const dataAccess = require('../data_access/dataAccess.js');


const router = express.Router();

function renderLogin(req, res, uzenet) {
  res.render('login', { session: req.session, uzenet });
}

router.get('/', (req, res) => {
  if (req.session.nev) res.redirect('/');
  else renderLogin(req, res, null);
});

router.post('/', (req, res) => {
  if (req.session.nev) {
    res.redirect('/');
    return;
  }
  const { nev, jelszo } = req.fields;
  dataAccess.getUser(nev, (err, results) => {
    if (err) renderLogin(req, res, 'Db hiba!');
    else if (results.length === 0) renderLogin(req, res, 'Nincs ilyen felhasznalo!');
    else {
      checkPassword(jelszo, results[0].so, results[0].jelszoHash, (error, siker) => {
        if (error) renderLogin(req, res, 'Nem vart hiba');
        else if (!siker) renderLogin(req, res, 'Hibas jelszo');
        else {
          req.session.nev = nev;
          req.session.szerepkor = results[0].szerepkor;
          res.redirect('/');
        }
      });
    }
  });
});

module.exports = router;
