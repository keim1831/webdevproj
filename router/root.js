const express = require('express');
const helpers = require('../helpers/helpers.js');
const csatlakozRouter = require('./connect.js');
const letrehozRouter = require('./create.js');
const regisztracioRouter = require('./registration.js');
const bejelentkezesRouter = require('./login.js');
const dataAccess = require('../data_access/dataAccess.js');
const authorize = require('../middleware/authorize.js');


const router = express.Router();

router.use('/connect', csatlakozRouter);
router.use('/create', letrehozRouter);
router.use('/registration', regisztracioRouter);
router.use('/login', bejelentkezesRouter);


function renderHomepage(req, res, rendezvenyek, aktualisUserSzervezi) {
  const szerevezettIdlista = aktualisUserSzervezi.map((x) => x.rendezvenyId);
  const rendezvenyView = rendezvenyek.map((r) => ({
    ...r,
    kezdet: r.kezdet.toLocaleDateString(),
    veg: r.veg.toLocaleDateString(),
    aktualisUserSzervezo: szerevezettIdlista.includes(r.id),
  }));
  res.render('homepage', { session: req.session, rendezveny: rendezvenyView });
}

router.get('/', (req, res) => {
  dataAccess.getEvents((err, rendezveny) => {
    if (err) res.render('homepage', { session: req.session, rendezveny: {}, uzenet: 'Db hiba!' });
    else if (!req.session.nev) {
      renderHomepage(req, res, rendezveny, []);
    } else {
      dataAccess.getEventsOrganizedBy(req.session.nev, (err2, lista) => {
        if (err2) res.status(500).send('DB hiba!');
        else renderHomepage(req, res, rendezveny, lista);
      });
    }
  });
});

function renderDetails(id, message, req, res) {
  dataAccess.getPictures(id, (err, fenykep) => {
    if (err) res.send(err);
    else {
      res.render('details', {
        session: req.session, id, fenykep, message,
      });
    }
  });
}

// a rendezvenyrol az adatok (fenykepek)
router.get('/event/:id', (req, res) => {
  const id  = +req.params.id;
  renderDetails(id, null, req, res);
});

// fenykep feltoltese
router.post('/edit', authorize(), (req, res) => {
  const rId = parseInt(req.fields.rendId, 10);
  const szNev = req.fields.sNev;

  helpers.editEvent(rId, szNev, req.files.f1, (status, result) => {
    if (status !== 200) {
      renderDetails(rId, result, req, res);
    } else {
      res.redirect(`/event/${rId}`);
    }
  });
});

router.get('/logout', (req, res) => {
  req.session.destroy(() => res.redirect('/'));
});

module.exports = router;
