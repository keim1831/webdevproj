const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'web_keim1831',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

function insertEvent(nev, kezdet, veg, helyszin, callback) {
  const query = 'INSERT INTO rendezveny (nev,kezdet,veg,helyszin) VALUES (?,?,?,?);';
  pool.query(query, [nev, kezdet, veg, helyszin], callback);
}

function getEvent(id, callback) {
  pool.query('SELECT * FROM rendezveny WHERE id = ?', [id], callback);
}

function updateEvent(id, nev, kezdet, veg, helyszin, callback) {
  pool.query('UPDATE rendezveny SET  nev = ?, kezdet = ?, veg = ?, helyszin = ? WHERE id = ?',
    [nev, kezdet, veg, helyszin, id], callback);
}

function isExistingEvent(rendezvenyId, callback) {
  getEvent(rendezvenyId, (err, results) => {
    if (err) callback(err, null);
    else callback(null, results.length > 0);
  });
}

function getEvents(callback) {
  pool.query('SELECT * FROM rendezveny', callback);
}

function deleteEvent(id, callback) {
  pool.query('DELETE FROM rendezvenynelSzervezo WHERE rendezvenyId = ?', [id], (err0) => {
    if (err0) callback(err0);
    else {
      pool.query('DELETE FROM fenykep WHERE rendezvenyId = ?', [id], (err1) => {
        if (err1) callback(err1);
        else pool.query('DELETE FROM rendezveny WHERE id = ?', [id], callback);
      });
    }
  });
}

function getEventByLocation(helyszin, callback) {
  pool.query('SELECT * FROM rendezveny WHERE helyszin = ?', [helyszin], callback);
}

// mar szervezo a rendezvenynel
function isOrganizer(rendezvenyId, szervezoNev, callback) {
  const query = `SELECT id FROM felhasznalo 
    JOIN rendezvenynelSzervezo rSz ON rSz.felhasznaloId = id 
    WHERE rSz.rendezvenyId = ? AND nev = ?;`;
  pool.query(query, [rendezvenyId, szervezoNev], (err, results) => {
    if (err) callback(err, null);
    else callback(null, results.length > 0);
  });
}

// a szervezo altal szervezett rendezvenyek
function getEventsOrganizedBy(userNev, callback) {
  const query = `SELECT rsz.rendezvenyId FROM felhasznalo 
    JOIN rendezvenynelSzervezo rSz ON rSz.felhasznaloId = id 
    WHERE nev = ?;`;
  pool.query(query, [userNev], callback);
}

function insertOrganizer(nev, rendezvenyId, callback) {
  const query = `INSERT INTO rendezvenynelSzervezo (felhasznaloId,rendezvenyId) 
    VALUES ((SELECT id FROM felhasznalo WHERE nev = ?),?);`;
  pool.query(query, [nev, rendezvenyId], callback);
}

function insertUser(nev, jelszoHash,  so, szerepkor, callback) {
  const query = 'INSERT INTO felhasznalo (nev, szerepkor, jelszoHash, so) VALUES (?,?,?,?);';
  pool.query(query, [nev, szerepkor, jelszoHash, so], callback);
}

function getUser(nev, callback) {
  const query = 'SELECT * FROM felhasznalo WHERE nev = ?;';
  pool.query(query, [nev], callback);
}

function usernameIsTaken(nev, callback) {
  getUser(nev, (err, results) => {
    if (err) callback(err, null);
    else callback(null, results.length > 0);
  });
}

function deleteOrganizer(nev, rendezvenyId, callback) {
  const query = `DELETE FROM rendezvenynelSzervezo 
    WHERE rendezvenyId = ? and felhasznaloId = (SELECT id FROM felhasznalo WHERE nev = ?);`;
  pool.query(query, [rendezvenyId, nev], callback);
}

function getUsers(callback) {
  pool.query('SELECT * FROM felhasznalo', callback);
}

function getEventOrganizer(rendezvenyId, callback) {
  const query = `SELECT * FROM felhasznalo
                JOIN rendezvenynelSzervezo as rsz on rsz.felhasznaloId = id 
                WHERE rendezvenyId = ?`;
  pool.query(query, [rendezvenyId], callback);
}

function insertPicture(fileNev, rendezvenyId, callback) {
  const query = 'INSERT INTO fenykep (fileNev,rendezvenyId) VALUES (?,?);';
  pool.query(query, [fileNev, rendezvenyId], callback);
}

function getPictures(rendezvenyId, callback) {
  pool.query('SELECT * FROM fenykep WHERE rendezvenyId = ?', [rendezvenyId], callback);
}

module.exports = {
  insertEvent,
  isOrganizer,
  isExistingEvent,
  insertOrganizer,
  deleteOrganizer,
  insertPicture,
  getEvents,
  deleteEvent,
  getUsers,
  getEvent,
  getPictures,
  insertUser,
  usernameIsTaken,
  getUser,
  getEventOrganizer,
  getEventsOrganizedBy,
  getEventByLocation,
  updateEvent,
};
