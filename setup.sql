DROP DATABASE IF EXISTS web_keim1831;
CREATE DATABASE web_keim1831;
USE web_keim1831;

-- készít egy felhasználót, aki minden műveletet végrehajthat ezen adatbázisban
CREATE USER IF NOT EXISTS 'webprog'@'localhost' IDENTIFIED BY 'VgJUjBd8';
GRANT ALL PRIVILEGES ON *.* TO 'webprog'@'localhost';

CREATE TABLE rendezveny (
    id INT AUTO_INCREMENT,
    nev VARCHAR(256),
    kezdet DATE,
    veg DATE,
    helyszin VARCHAR(256),
    PRIMARY KEY (id)
);

CREATE TABLE felhasznalo (
    id INT AUTO_INCREMENT,
    nev VARCHAR(256),
    szerepkor VARCHAR(50),
    jelszoHash VARCHAR (100),
    so VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE rendezvenynelSzervezo(
    felhasznaloId INT,
    rendezvenyId INT,
    FOREIGN KEY (felhasznaloId) REFERENCES felhasznalo(id),
    FOREIGN KEY (rendezvenyId) REFERENCES rendezveny(id),
    PRIMARY KEY (felhasznaloId,rendezvenyId)
);

CREATE TABLE fenykep (
    id INT AUTO_INCREMENT,
    fileNev VARCHAR(256),
    rendezvenyId INT,
    PRIMARY KEY (id),
    FOREIGN KEY (rendezvenyId) REFERENCES rendezveny(id)
);
