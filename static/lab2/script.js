/* eslint-disable operator-linebreak */
/* eslint-disable indent */
/* eslint-disable space-before-blocks */
/* eslint-disable no-unused-vars */
let ok = true;
let ok2 = true;

function onBlur() {
    const validMailFormat = /^.+@(yahoo|gmail)\..{2,6}$/;
    const myInput = document.getElementById('email');
    const errorEmailSpan = document.getElementById('error1');

    if (!myInput.value.match(validMailFormat)) {
        errorEmailSpan.innerHTML =
        'Ervenytelen e-mail cim!Csak gmail vagy yahoo email cimet irhat be!';
        errorEmailSpan.className += ' error-shown';
        ok = false;
    } else {
        errorEmailSpan.innerHTML = '';
        errorEmailSpan.className = 'error-hidden';
        ok = true;
    }
}

function webPageValidation() {
    const validWebPageFormat = /^(www.)?[a-zA-Z0-9_-]+\.([a-zA-Z0-9_-]+\.)+[a-z]{2,6}$/;
    const myInput = document.getElementById('web');
    const errorWebPageSpan = document.getElementById('error2');

    if (!myInput.value.match(validWebPageFormat)) {
        errorWebPageSpan.innerHTML = 'Hiba! Nem megfelelo weboldal !';
        errorWebPageSpan.className += ' error-shown';
        ok2 = false;
    } else {
        errorWebPageSpan.innerHTML = '';
        errorWebPageSpan.className = 'error-hidden';
        ok2 = true;
    }
}

function lastModify() {
    if (document.lastModified) {
        document.getElementById('date').innerHTML = document.lastModified.toLocaleString();
    }
}


function passwordCheck() {
    const myInput = document.getElementById('psw');
    const kicsiBetusSpan = document.getElementById('kicsi');
    const hibaSpan = document.getElementById('pwdErr');
    const formatum = /(\W{1,2}|[A-Z]+[a-z]+[0-9]+)/;

    console.log(myInput);
    kicsiBetusSpan.innerHTML = myInput.value;

    let hibaUzenet = '';

    if (myInput.value.length < 5) {
        hibaUzenet += 'Tul rovid!';
    }
    if (myInput.value.length > 12) {
        hibaUzenet += 'Tul hosszu jelszo! ';
    }
    if (!myInput.value.match(formatum)) {
        hibaUzenet += 'legalább egy, legtöbb két speciális karakter'
        + '(ami nem betű és nem szám vagy alulvonás) kell'
        + 'szerepeljen benne, vagy legyen benne nagy- és kisbetű, illetve számjegy is';
        hibaSpan.className += ' error-shown';
    }
        hibaSpan.innerHTML = hibaUzenet;
}

function validateButtonPress() {
    onBlur();
    webPageValidation();
    passwordCheck();
    const msg = document.getElementById('message');
    if (ok === true && ok2 === true){
        msg.innerHTML = 'Minden OK';
        document.getElementById('submit').disabled = false;
    }
    const form = document.getElementById('form');
    console.log(form.validate());
}

let interval = null;


function stop() {
    if (interval) {
        clearInterval(interval);
        interval = null;
    }
}


function toRight() {
    stop();

    const container = document.getElementById('container');
    const containerRect = container.getBoundingClientRect();
    container.innerHTML = '';

    const szoveg = document.getElementById('szoveg').value;

    if (!szoveg) return;

    const span1 = document.createElement('span');
    span1.innerHTML = szoveg;
    span1.style.position = 'absolute';
    span1.style.paddingRight = '50px';
    span1.style.left = '0px';

    container.appendChild(span1);

    const contMeret = containerRect.width;
    const meret = span1.getBoundingClientRect().width;

    const hanyados = Math.floor(contMeret / meret);
    const maradek = contMeret - (hanyados * meret);
    const hanyszor = hanyados + 1;
    const javitas = maradek / hanyados;
    const javitottMeret = javitas + meret;

    const spans = [span1];

    for (let i = 1; i < hanyszor; i += 1) {
        const span = document.createElement('span');
        span.innerHTML = szoveg;
        span.style.position = 'absolute';
        span.style.paddingRight = '50px';
        span.style.left = `${-i * javitottMeret}px`;
        spans.push(span);
        container.appendChild(span);
    }

    const speed = 2;

    interval = setInterval(() => {
        for (let i = 0; i < spans.length; i += 1) {
            const span = spans[i];
            const old = parseFloat(span.style.left);
            let newValue = old + speed;
            if (newValue >= contMeret) {
                newValue = -javitottMeret;
            }

            // console.log("old: ", old, "new: ", newValue);
            span.style.left = `${newValue}px`;
        }
    }, 10);
}

function toLeft() {
    stop();

    const container = document.getElementById('container');
    const containerRect = container.getBoundingClientRect();
    container.innerHTML = '';

    const szoveg = document.getElementById('szoveg').value;

    if (!szoveg) return;

    const span1 = document.createElement('span');
    span1.innerHTML = szoveg;
    span1.style.position = 'absolute';
    span1.style.paddingLeft = '50px';


    container.appendChild(span1);

    const contMeret = containerRect.width;
    const meret = span1.getBoundingClientRect().width;
    const hanyados = Math.floor(contMeret / meret);
    const maradek = contMeret - (hanyados * meret);
    const hanyszor = hanyados + 1;
    const javitas = maradek / hanyados;
    const javitottMeret = javitas + meret;
    span1.style.left = `${contMeret - javitottMeret}px`;

    const spans = [span1];

    for (let i = 1; i < hanyszor; i += 1) {
        const span = document.createElement('span');
        span.innerHTML = szoveg;
        span.style.position = 'absolute';
        span.style.paddingLeft = '50px';
        span.style.left = `${contMeret + (i - 1) * javitottMeret}px`;

        spans.push(span);
        container.appendChild(span);
    }


    const speed = 2;

    interval = setInterval(() => {
        for (let i = 0; i < spans.length; i += 1) {
            const span = spans[i];
            const old = parseFloat(span.style.left);
            let newValue = old - speed;
            if (newValue <= -javitottMeret) {
                newValue = contMeret;
            }
            span.style.left = `${newValue}px`;
        }
    }, 10);
}

function start() {
    const irany = document.getElementById('irany').value;
    if (irany === 'jobb') {
        toRight();
    } else {
        toLeft();
    }
}
