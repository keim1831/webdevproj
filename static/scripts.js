function displayStaff(names, id) {
  const div = document.getElementById(`staff-${id}`);
  div.style.display = 'block';
  div.innerHTML = `<b>Szervezok: </b>${names.join(', ')}`;
}

// eslint-disable-next-line no-unused-vars
function loadStaff(id) {
  fetch(`/api/organisational-relationships?rendezvenyId=${id}`)
    .then((response) => response.json())
    .then((nevek) => displayStaff(nevek, id))
    .catch((err) => console.error(err));
}

// eslint-disable-next-line no-unused-vars
function connect(id) {
  fetch('/api/organisational-relationships', {
    method: 'POST',
    body: JSON.stringify({ rendezvenyId: id }),
    headers: { 'Content-Type': 'application/json' },
  })
    .then((response) => {
      if (response.ok) {
        const div = document.getElementById(`join-${id}`);
        div.innerHTML = `<button onclick="disconnect(${id})">Visszalepek</button>`;
        const staffDiv = document.getElementById(`staff-${id}`);
        if (staffDiv.innerHTML.length) loadStaff(id);
        document.getElementById(`error-${id}`).innerHTML = 'Sikerült csatlakozni!';
      } else {
        document.getElementById(`error-${id}`).innerHTML = 'Sikertelen csatlakozás!';
      }
    }).catch((error) => console.log(error));
}

// eslint-disable-next-line no-unused-vars
function disconnect(id) {
  fetch(`/api/organisational-relationships/${id}`, { method: 'DELETE' })
    .then((response) => {
      if (response.ok) {
        const div = document.getElementById(`join-${id}`);
        div.innerHTML = `<button onclick="connect(${id})">Csatlakozom</button>`;
        const staffDiv = document.getElementById(`staff-${id}`);
        if (staffDiv.innerHTML.length) loadStaff(id);
        document.getElementById(`error-${id}`).innerHTML = 'Sikerült visszalépni!';
      } else {
        document.getElementById(`error-${id}`).innerHTML = 'Sikertelen visszalépés!';
      }
    }).catch((error) => console.log(error));
}
