const fs = require('fs');
const path = require('path');

const random = require('./randomString.js');
const dataAccess = require('../data_access/dataAccess.js');

const uploadsDir = path.join(path.resolve(__dirname), '..', 'static', 'uploads');
try {
  fs.statSync(uploadsDir);
} catch (e) {
  fs.mkdirSync(uploadsDir);
}

function getDate(str) {
  const date = str ? new Date(str) : new Date();
  date.setHours(0, 0, 0, 0);
  return date;
}

function insertEvent(rendezveny, callback) {
  const today = getDate();
  const startDate = getDate(rendezveny.date1);
  const endDate =  getDate(rendezveny.date2);

  if (startDate.getTime() >= today.getTime() && startDate.getTime() <= endDate.getTime()) {
    dataAccess.insertEvent(rendezveny.nev, startDate, endDate, rendezveny.helyszin, (err) => {
      if (err) {
        callback(500, 'Db hiba!');
      } else {
        callback(200, 'Sikerult hozzaadni a rendezvenyt.');
      }
    });
  } else {
    callback(400, 'hibas datum');
  }
}

function isOrganizer(rid, szNev, callback) {
  dataAccess.isOrganizer(rid, szNev, (err, szervezoE) => {
    if (err) callback(500, 'Db hiba!');
    else callback(200, szervezoE);
  });
}

function isExistingEvent(rid, callback) {
  dataAccess.isExistingEvent(rid, (err, letezikE) => {
    if (err) callback(500, err);
    else callback(200, letezikE);
  });
}

// beszurja a szervezot, ha megfelelo hibaellenorzeseken vegigment
function insertOrganizer(request, callback) {
  const id = parseInt(request.rId, 10);
  const szNev = request.nev;
  isExistingEvent(id, (status, letezikRendezveny) => {
    if (status === 500) callback(500, 'Db hiba!');
    else if (!letezikRendezveny) callback(403, 'Rendezveny nem letezik.');
    else { // rendeveny letezik
      isOrganizer(id, szNev, (stat, szervezoE) => {
        if (stat === 500) callback(500, 'Db hiba!');
        else if (szervezoE) callback(403, 'Mar szervezo!');
        else { // meg nem szervezo
          dataAccess.insertOrganizer(szNev, id, (err) => {
            if (err) callback(500, 'Db hiba!');
            else callback(200, 'Sikeresense hozzadva!');
          });
        }
      });
    }
  });
}

// torli a szervezot, ha szervezo volt a kivalasztott rendezvenynel
function deleteOrganizer(request, callback) {
  const id = parseInt(request.rId, 10);
  const szNev = request.nev;
  isExistingEvent(id, (status, letezikRendezveny) => {
    if (status === 500) callback(500, 'db hiba');
    else if (!letezikRendezveny) callback(403, 'rendezveny nem letezik');
    else { // rendeveny letezik
      isOrganizer(id, szNev, (stat, szervezo) => {
        if (stat === 500) callback(500, 'db hiba');
        else if (!szervezo) callback(403, 'meg nem szervezo');
        else { // szervezo volt
          dataAccess.deleteOrganizer(szNev, id, (err) => {
            if (err) callback(500, 'db hiba');
            else callback(200, 'sikeresense torolve');
          });
        }
      });
    }
  });
}

function moveFileToUploads(file, callback) {
  const ujNev = `${random(6)}_${file.name}`;
  fs.copyFile(file.path, path.join(uploadsDir, ujNev), () => {
    fs.unlink(file.path, () => callback(ujNev));
  });
}

function insertPicture(ujNev, rId, file, callback) {
  dataAccess.insertPicture(ujNev, rId, (err) => {
    if (err) callback(500, 'Db hiba!');
    else callback(200, null);
  });
}


// feltolti a kepet, majd fbeszurja a nevet a fenykep tablaba
function editEvent(rId, szNev, file, callback) {
  isExistingEvent(rId, (status, letezikRendezveny) => {
    if (status === 500) callback(500, 'Db hiba!');
    else if (!letezikRendezveny) callback(404, 'Rendezveny nem letezik!');
    else { // rendeveny letezik
      isOrganizer(rId, szNev, (stat, szervezo) => {
        if (stat === 500) callback(500, 'Db hiba!');
        else if (!szervezo) callback(403, 'Meg nem volt szervezo!');
        else { // szervezo volt
          moveFileToUploads(file, (ujNev) => insertPicture(ujNev, rId, file, callback));
        }
      });
    }
  });
}


module.exports = {
  insertEvent,
  insertOrganizer,
  deleteOrganizer,
  editEvent,
};
