const crypto = require('crypto');

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

// generálunk hash-t egy jelszóból
function hashPassord(password, callback) {
  // só
  crypto.randomBytes(saltSize, (err, salt) => {
    if (err) callback(err, null, null);
    else {
      // hash készítése
      crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
        if (cryptErr) callback(cryptErr, null, null);
        else callback(null, salt.toString('hex'), hash.toString('hex'));
      });
    }
  });
}

// ellenőrizzük egy megadott jelszóról hogy megfelel-e
// egy megadott hashnek
function checkPassword(password, salt, hash, callback) {
  const binarySalt = Buffer.from(salt, 'hex');

  crypto.pbkdf2(password, binarySalt, iterations, hashSize, hashAlgorithm, (err, binaryHash) => {
    if (err) callback(err, false);
    else {
      const actualHash = binaryHash.toString('hex');
      callback(null, hash === actualHash);
    }
  });
}

module.exports = {
  hashPassord,
  checkPassword,
};
